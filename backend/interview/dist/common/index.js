"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const level1_1 = __importDefault(require("../data/level1"));
const level2_1 = __importDefault(require("../data/level2"));
const level3_1 = __importDefault(require("../data/level3"));
let method = {
    // function for finding article values
    searchArticle: function (article_id, level) {
        if (level == 1) {
            let result = level1_1.default.articles.filter(function (article) {
                return article.id == article_id;
            });
            return result[0];
        }
        if (level == 2) {
            let result = level2_1.default.articles.filter(function (article) {
                return article.id == article_id;
            });
            return result[0];
        }
        if (level == 3) {
            let result = level3_1.default.articles.filter(function (article) {
                return article.id == article_id;
            });
            return result[0];
        }
    },
    // function for mapping delivery fees
    deliveryFees: function (total) {
        let result = level2_1.default.delivery_fees.filter(function (fees) {
            return (total >= fees.eligible_transaction_volume.min_price && (total < fees.eligible_transaction_volume.max_price || fees.eligible_transaction_volume.max_price == null));
        });
        return result[0];
    },
    // function for getting Discounts fees
    discounts: function (article_id) {
        let result = level3_1.default.discounts.filter(function (discount) {
            return discount.article_id == article_id;
        });
        return result[0];
    }
};
module.exports = method;
//# sourceMappingURL=index.js.map