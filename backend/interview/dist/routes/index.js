"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const level1_1 = __importDefault(require("../data/level1"));
const level2_1 = __importDefault(require("../data/level2"));
const level3_1 = __importDefault(require("../data/level3"));
let express = require("express");
let router = express.Router();
let cartController = require("../controller/cart.contoller");
// routes for request data methods
router.route('/level1/data').get((req, res) => {
    res.json(level1_1.default);
});
router.route('/level2/data').get((req, res) => {
    res.json(level2_1.default);
});
router.route('/level3/data').get((req, res) => {
    res.json(level3_1.default);
});
// routes for response data methods
router.route('/level1/output').post(cartController.level1Cart);
router.route('/level2/output').post(cartController.level2Cart);
router.route('/level3/output').post(cartController.level3Cart);
module.exports = router;
//# sourceMappingURL=index.js.map