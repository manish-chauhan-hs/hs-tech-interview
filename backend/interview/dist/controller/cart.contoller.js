'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const level1_1 = __importDefault(require("../data/level1"));
const level2_1 = __importDefault(require("../data/level2"));
const level3_1 = __importDefault(require("../data/level3"));
let commonMethod = require("../common/index");
module.exports.level1Cart = function (req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        let carts = [];
        try {
            // assign data to a variable
            let allData = level1_1.default;
            if (allData) {
                yield allData.carts.forEach(cart => {
                    let cartTotal = 0; // initialize total of each cart
                    // fetching all cart's item one by one
                    cart.items.forEach(item => {
                        // call searchArticle with article_id and assign to variable
                        let article = commonMethod.searchArticle(item.article_id, 1);
                        cartTotal += article.price * item.quantity; // total calculation
                    });
                    // push values into carts array
                    carts.push({
                        "id": cart.id,
                        "total": cartTotal
                    });
                });
                return res.json({ status: true, message: 'success', data: carts });
            }
            else {
                return res.json({ status: false, data: "data not found" });
            }
        }
        catch (e) {
            console.log(e);
            return res.json({ status: false, data: "something went wrong" });
        }
    });
};
module.exports.level2Cart = function (req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        let carts = [];
        try {
            // assign data to a variable
            let allData = level2_1.default;
            if (allData) {
                yield allData.carts.forEach(cart => {
                    let cartTotal = 0; // initialize total of each cart
                    // fetching all cart's item one by one
                    cart.items.forEach(item => {
                        // call searchArticle with article_id and assign to variable
                        let article = commonMethod.searchArticle(item.article_id, 2);
                        cartTotal += article.price * item.quantity; // total calculation
                    });
                    // call deliveryFees with total values for delivery fees calculation
                    let fees = commonMethod.deliveryFees(cartTotal);
                    cartTotal += fees.price; // add delivery fees to total
                    // push values into carts array
                    carts.push({
                        "id": cart.id,
                        "total": cartTotal
                    });
                });
                return res.json({ status: true, message: 'success', data: carts });
            }
            else {
                return res.json({ status: false, data: "data not found" });
            }
        }
        catch (e) {
            console.log(e);
            return res.json({ status: false, data: "something went wrong" });
        }
    });
};
module.exports.level3Cart = function (req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        let carts = [];
        try {
            // assign data to a variable
            let allData = level3_1.default;
            if (allData) {
                yield allData.carts.forEach(cart => {
                    let cartTotal = 0; // initialize total of each cart
                    // fetching all cart's item one by one
                    cart.items.forEach(item => {
                        let discount = 0; // initialize discount on each iteration
                        // call searchArticle with article_id and assign to variable
                        let article = commonMethod.searchArticle(item.article_id, 3);
                        let discountData = commonMethod.discounts(item.article_id); // call discounts with article_id to get discount amount of article
                        let tempTotal = article.price * item.quantity; // assign total value into temp variable
                        if (discountData) {
                            // check type of discount
                            if (discountData.type === 'amount') {
                                discount = discountData.value * item.quantity; // calculate discount of type amount
                            }
                            else {
                                discount = Math.round(tempTotal * discountData.value / 100); // calculate discount of type percentage
                            }
                        }
                        cartTotal += tempTotal - discount; // total calculation
                    });
                    // call deliveryFees with total values for delivery fees calculation
                    let fees = commonMethod.deliveryFees(cartTotal);
                    cartTotal += fees.price; // add delivery fees to total
                    // push values into carts array
                    carts.push({
                        "id": cart.id,
                        "total": cartTotal
                    });
                });
                return res.json({ status: true, message: 'success', data: carts });
            }
            else {
                return res.json({ status: false, data: "data not found" });
            }
        }
        catch (e) {
            console.log(e);
            return res.json({ status: false, data: "something went wrong" });
        }
    });
};
//# sourceMappingURL=cart.contoller.js.map