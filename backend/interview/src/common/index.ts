import data1 from '../data/level1';
import data2 from '../data/level2';
import data3 from '../data/level3';

let method = {
    // function for finding article values
    searchArticle : function (article_id, level) {
        if(level == 1){
            let result = data1.articles.filter(function (article) {
                return article.id == article_id;
            });
            return result[0];
        }
    
        if(level == 2){
            let result = data2.articles.filter(function (article) {
                return article.id == article_id;
            });
            return result[0];
        }

        if(level == 3){
            let result = data3.articles.filter(function (article) {
                return article.id == article_id;
            });
            return result[0];
        }
    },
    
    // function for mapping delivery fees
    deliveryFees : function (total) {
        let result = data2.delivery_fees.filter(function (fees) {
            return (total >= fees.eligible_transaction_volume.min_price && (total < fees.eligible_transaction_volume.max_price || fees.eligible_transaction_volume.max_price == null));
        });
        return result[0];
    },

    // function for getting Discounts fees
    discounts : function (article_id) {
        let result = data3.discounts.filter(function (discount) {
            return discount.article_id == article_id;
        });
        return result[0];
    }
};

module.exports = method;