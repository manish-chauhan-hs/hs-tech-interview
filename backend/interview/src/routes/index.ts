import data1 from '../data/level1';
import data2 from '../data/level2';
import data3 from '../data/level3';

let express = require("express");
let router = express.Router();
let cartController = require("../controller/cart.contoller");

// routes for request data methods
router.route('/level1/data').get( (req, res) => {
    res.json(data1);
});

router.route('/level2/data').get( (req, res) => {
    res.json(data2);
});

router.route('/level3/data').get( (req, res) => {
    res.json(data3);
});

// routes for response data methods
router.route('/level1/output').post(cartController.level1Cart);
router.route('/level2/output').post(cartController.level2Cart);  
router.route('/level3/output').post(cartController.level3Cart);    

export = router;