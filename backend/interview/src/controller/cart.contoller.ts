'use strict';

import data1 from '../data/level1';
import data2 from '../data/level2';
import data3 from '../data/level3';
let commonMethod = require("../common/index");

module.exports.level1Cart = async function(req, res) {
    let carts = [];
    try {
        // assign data to a variable
        let allData = data1;
    
        if(allData){
            await allData.carts.forEach( cart => {
                let cartTotal= 0; // initialize total of each cart

                // fetching all cart's item one by one
               cart.items.forEach(item => {
                    // call searchArticle with article_id and assign to variable
                     let article = commonMethod.searchArticle(item.article_id,1);
                     cartTotal += article.price * item.quantity; // total calculation
               });

                // push values into carts array
                carts.push({
                    "id" : cart.id,
                    "total" : cartTotal
                });
            });

            return res.json({status: true, message: 'success', data: carts});
        } else {
            return res.json({status: false, data: "data not found"});
        }
    }
    catch (e) {
        console.log(e);
        return res.json({status: false, data: "something went wrong"});
    }    
};

module.exports.level2Cart = async function(req, res) {
    let carts = [];
    try {
        // assign data to a variable
        let allData = data2;

        if(allData){
            await allData.carts.forEach( cart => {
                let cartTotal= 0; // initialize total of each cart

                // fetching all cart's item one by one
                cart.items.forEach(item => {
                    // call searchArticle with article_id and assign to variable
                    let article = commonMethod.searchArticle(item.article_id,2);
                    cartTotal += article.price * item.quantity; // total calculation
                });

                // call deliveryFees with total values for delivery fees calculation
                let fees = commonMethod.deliveryFees(cartTotal);
                cartTotal += fees.price; // add delivery fees to total

                // push values into carts array
                carts.push({
                    "id" : cart.id,
                    "total" : cartTotal
                });
            });
            return res.json({status: true, message: 'success', data: carts});
        } else {
            return res.json({status: false, data: "data not found"});
        }
    }
    catch (e) {
      console.log(e);
      return res.json({status: false, data: "something went wrong"});
    }    
};

module.exports.level3Cart = async function(req, res) {
    let carts = [];
    try {
        // assign data to a variable
        let allData = data3;
       
       if(allData){
           await allData.carts.forEach( cart => {
               let cartTotal= 0; // initialize total of each cart

                // fetching all cart's item one by one
                cart.items.forEach(item => {
                    let discount = 0; // initialize discount on each iteration

                    // call searchArticle with article_id and assign to variable
                    let article = commonMethod.searchArticle(item.article_id,3);
                    let discountData = commonMethod.discounts(item.article_id); // call discounts with article_id to get discount amount of article
                    let tempTotal = article.price * item.quantity; // assign total value into temp variable

                    if(discountData) {
                        // check type of discount
                        if(discountData.type === 'amount') {
                            discount = discountData.value * item.quantity; // calculate discount of type amount
                        } else {
                            discount = Math.round(tempTotal * discountData.value / 100); // calculate discount of type percentage
                        }
                    }
                    cartTotal += tempTotal - discount; // total calculation
                });

                // call deliveryFees with total values for delivery fees calculation
                let fees = commonMethod.deliveryFees(cartTotal);
                cartTotal += fees.price; // add delivery fees to total

                // push values into carts array
                carts.push({
                    "id" : cart.id,
                    "total" : cartTotal
                });
            });
            return res.json({status: true, message: 'success',data: carts});
        } else {
            return res.json({status: false, data: "data not found"});
        }
    }
    catch (e) {
        console.log(e);
        return res.json({status: false, data: "something went wrong"});
    }  
};