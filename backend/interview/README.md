﻿## Project User Manual

Steps to run application :

**npm install** - Install node modules

**npm start** - Run application using build created

## Level 1

Show input data for level 1 : 
GET `/level1/data`

JSON input:
```javascript
{
 "articles": [
   { "id": 1, "name": "water", "price": 100 },
   { "id": 2, "name": "honey", "price": 200 },
   { "id": 3, "name": "mango", "price": 400 },
   { "id": 4, "name": "tea", "price": 1000 }
 ],
 "carts": [
   {
     "id": 1,
     "items": [
       { "article_id": 1, "quantity": 6 },
       { "article_id": 2, "quantity": 2 },
       { "article_id": 4, "quantity": 1 }
     ]
   },
   {
     "id": 2,
     "items": [
       { "article_id": 2, "quantity": 1 },
       { "article_id": 3, "quantity": 3 }
     ]
   },
   {
     "id": 3,
     "items": []
   }
 ]
}
```


### Output:

Show output data for level 1 :
POST `/level1/output` 

JSON Output:
```javascript
{
    "status": true,
    "message": "All the cart total",
    "data": [
        {
            "id": 1,
            "total": 2000
        },
        {
            "id": 2,
            "total": 1400
        },
        {
            "id": 3,
            "total": 0
        }
    ]
}
```


## Level 2

Show input data for level 2 :
GET `/level2/data`

JSON input:
```javascript
{
 "articles": [
   { "id": 1, "name": "water", "price": 100 },
   { "id": 2, "name": "honey", "price": 200 },
   { "id": 3, "name": "mango", "price": 400 },
   { "id": 4, "name": "tea", "price": 1000 }
 ],
 "carts": [
   {
     "id": 1,
     "items": [
       { "article_id": 1, "quantity": 6 },
       { "article_id": 2, "quantity": 2 },
       { "article_id": 4, "quantity": 1 }
     ]
   },
   {
     "id": 2,
     "items": [
       { "article_id": 2, "quantity": 1 },
       { "article_id": 3, "quantity": 3 }
     ]
   },
   {
     "id": 3,
     "items": []
   }
 ],
 "delivery_fees": [
   {
     "eligible_transaction_volume": {
       "min_price": 0,
       "max_price": 1000
     },
     "price": 800
   },
   {
     "eligible_transaction_volume": {
       "min_price": 1000,
       "max_price": 2000
     },
     "price": 400
   },
   {
     "eligible_transaction_volume": {
       "min_price": 2000,
       "max_price": null
     },
     "price": 0
   }
 ]
}
```

### Output:

Show output data for level 2 :
POST `/level2/output`

JSON Output:
```javascript
{
    "status": true,
    "message": "All the cart total",
    "data": [
        {
            "id": 1,
            "total": 2000
        },
        {
            "id": 2,
            "total": 1800
        },
        {
            "id": 3,
            "total": 800
        }
    ]
}
```


## Level 3

Show input data for level 3 :
GET `/level3/data`

JSON input:
```javascript
{
 "articles": [
   { "id": 1, "name": "water", "price": 100 },
   { "id": 2, "name": "honey", "price": 200 },
   { "id": 3, "name": "mango", "price": 400 },
   { "id": 4, "name": "tea", "price": 1000 },
   { "id": 5, "name": "ketchup", "price": 999 },
   { "id": 6, "name": "mayonnaise", "price": 999 },
   { "id": 7, "name": "fries", "price": 378 },
   { "id": 8, "name": "ham", "price": 147 }
 ],
 "carts": [
   {
     "id": 1,
     "items": [
       { "article_id": 1, "quantity": 6 },
       { "article_id": 2, "quantity": 2 },
       { "article_id": 4, "quantity": 1 }
     ]
   },
   {
     "id": 2,
     "items": [
       { "article_id": 2, "quantity": 1 },
       { "article_id": 3, "quantity": 3 }
     ]
   },
   {
     "id": 3,
     "items": [
       { "article_id": 5, "quantity": 1 },
       { "article_id": 6, "quantity": 1 }
     ]
   },
   {
     "id": 4,
     "items": [
       { "article_id": 7, "quantity": 1 }
     ]
   },
   {
     "id": 5,
     "items": [
       { "article_id": 8, "quantity": 3 }
     ]
   }
 ],
 "delivery_fees": [
   {
     "eligible_transaction_volume": {
       "min_price": 0,
       "max_price": 1000
     },
     "price": 800
   },
   {
     "eligible_transaction_volume": {
       "min_price": 1000,
       "max_price": 2000
     },
     "price": 400
   },
   {
     "eligible_transaction_volume": {
       "min_price": 2000,
       "max_price": null
     },
     "price": 0
   }
 ],
 "discounts": [
   { "article_id": 2, "type": "amount", "value": 25 },
   { "article_id": 5, "type": "percentage", "value": 30 },
   { "article_id": 6, "type": "percentage", "value": 30 },
   { "article_id": 7, "type": "percentage", "value": 25 },
   { "article_id": 8, "type": "percentage", "value": 10 }
 ]
}
```

### Output:

Show output data for level 2 :
POST `/level3/output`

JSON Output:
```javascript
{
    "status": true,
    "message": "All the cart total",
    "data": [
        {
            "id": 1,
            "total": 2350
        },
        {
            "id": 2,
            "total": 1775
        },
        {
            "id": 3,
            "total": 1798
        },
        {
            "id": 4,
            "total": 1083
        },
        {
            "id": 5,
            "total": 1197
        }
    ]
}
```


## Project Structure

#### Folder Structure

  + dist  - contains build of app built via typescript
  + src
    + common - contains common methods of app
    + controller - contains controller files of app
    + data - contains input json data
    + routes - contains app routes file
    + app.ts - starting point of app
  
 
